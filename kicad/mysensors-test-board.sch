EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:arduino
LIBS:nrf24l01
LIBS:irm-05-5
LIBS:lm3940
LIBS:mcp1702
LIBS:mysensors-test-board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L NRF24L01 U4
U 1 1 59907F10
P 8150 2800
F 0 "U4" H 8150 2800 60  0000 C CNN
F 1 "NRF24L01" H 8175 3500 60  0000 C CNN
F 2 "myModules:NRF24L01" H 8150 2800 60  0001 C CNN
F 3 "" H 8150 2800 60  0000 C CNN
	1    8150 2800
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR01
U 1 1 59907F81
P 8700 3550
F 0 "#PWR01" H 8700 3300 50  0001 C CNN
F 1 "GND" H 8700 3400 50  0000 C CNN
F 2 "" H 8700 3550 50  0000 C CNN
F 3 "" H 8700 3550 50  0000 C CNN
	1    8700 3550
	1    0    0    -1  
$EndComp
$Comp
L arduino_mini U1
U 1 1 5992F159
P 4750 3600
F 0 "U1" H 4775 4150 70  0000 C CNN
F 1 "arduino_mini" H 4775 3900 70  0000 C CNN
F 2 "myMicroControllers:Arduino-mini" H 4750 4700 60  0001 C CNN
F 3 "" H 5075 3100 60  0000 C CNN
	1    4750 3600
	-1   0    0    1   
$EndComp
$Comp
L CP C1
U 1 1 5AB60BD4
P 5600 6050
F 0 "C1" H 5625 6150 50  0000 L CNN
F 1 "10µF 6.3V" H 5400 5950 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2" H 5638 5900 50  0001 C CNN
F 3 "" H 5600 6050 50  0000 C CNN
	1    5600 6050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5AB60C12
P 5600 6400
F 0 "#PWR02" H 5600 6150 50  0001 C CNN
F 1 "GND" H 5600 6250 50  0000 C CNN
F 2 "" H 5600 6400 50  0000 C CNN
F 3 "" H 5600 6400 50  0000 C CNN
	1    5600 6400
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR03
U 1 1 5AB63038
P 2100 3750
F 0 "#PWR03" H 2100 3600 50  0001 C CNN
F 1 "+3.3V" H 2100 3890 50  0000 C CNN
F 2 "" H 2100 3750 50  0000 C CNN
F 3 "" H 2100 3750 50  0000 C CNN
	1    2100 3750
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR04
U 1 1 5AB630B8
P 5600 5700
F 0 "#PWR04" H 5600 5550 50  0001 C CNN
F 1 "+3.3V" H 5600 5840 50  0000 C CNN
F 2 "" H 5600 5700 50  0000 C CNN
F 3 "" H 5600 5700 50  0000 C CNN
	1    5600 5700
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR05
U 1 1 5AB6318C
P 7600 2800
F 0 "#PWR05" H 7600 2650 50  0001 C CNN
F 1 "+3.3V" H 7600 2940 50  0000 C CNN
F 2 "" H 7600 2800 50  0000 C CNN
F 3 "" H 7600 2800 50  0000 C CNN
	1    7600 2800
	1    0    0    -1  
$EndComp
$Comp
L Jumper_NC_Small JP1
U 1 1 5AB65902
P 4650 6300
F 0 "JP1" H 4650 6380 50  0000 C CNN
F 1 "Measure" H 4660 6240 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 4650 6300 50  0001 C CNN
F 3 "" H 4650 6300 50  0000 C CNN
	1    4650 6300
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X12 P1
U 1 1 5AD778DD
P 2850 1500
F 0 "P1" H 2850 2150 50  0000 C CNN
F 1 "ARDUINO1" V 2950 1500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x12" H 2850 1500 50  0001 C CNN
F 3 "" H 2850 1500 50  0000 C CNN
	1    2850 1500
	0    1    -1   0   
$EndComp
$Comp
L CONN_01X12 P3
U 1 1 5AD77AA6
P 6550 1500
F 0 "P3" H 6550 2150 50  0000 C CNN
F 1 "ARDUINO2" V 6650 1500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x12" H 6550 1500 50  0001 C CNN
F 3 "" H 6550 1500 50  0000 C CNN
	1    6550 1500
	0    1    -1   0   
$EndComp
$Comp
L CONN_01X02 P2
U 1 1 5AD781E3
P 3500 6050
F 0 "P2" H 3500 6200 50  0000 C CNN
F 1 "POWER" V 3600 6050 50  0000 C CNN
F 2 "myConnectors:NINIGI_NS25-W2P" H 3500 6050 50  0001 C CNN
F 3 "" H 3500 6050 50  0000 C CNN
	1    3500 6050
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X04 P4
U 1 1 5AD7B475
P 4750 1500
F 0 "P4" H 4750 1750 50  0000 C CNN
F 1 "ARDUINO3" V 4850 1500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 4750 1500 50  0001 C CNN
F 3 "" H 4750 1500 50  0000 C CNN
	1    4750 1500
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X08 P5
U 1 1 5AD7B6E6
P 2500 5450
F 0 "P5" H 2500 5900 50  0000 C CNN
F 1 "GROUND" V 2600 5450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08" H 2500 5450 50  0001 C CNN
F 3 "" H 2500 5450 50  0000 C CNN
	1    2500 5450
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR06
U 1 1 5AD7B7DB
P 2800 6000
F 0 "#PWR06" H 2800 5750 50  0001 C CNN
F 1 "GND" H 2800 5850 50  0000 C CNN
F 2 "" H 2800 6000 50  0000 C CNN
F 3 "" H 2800 6000 50  0000 C CNN
	1    2800 6000
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X08 P6
U 1 1 5AD7BC89
P 6700 5450
F 0 "P6" H 6700 5900 50  0000 C CNN
F 1 "POWER-OUT" V 6800 5450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08" H 6700 5450 50  0001 C CNN
F 3 "" H 6700 5450 50  0000 C CNN
	1    6700 5450
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR07
U 1 1 5AD7BE8B
P 6400 5000
F 0 "#PWR07" H 6400 4850 50  0001 C CNN
F 1 "+3.3V" H 6400 5140 50  0000 C CNN
F 2 "" H 6400 5000 50  0000 C CNN
F 3 "" H 6400 5000 50  0000 C CNN
	1    6400 5000
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR08
U 1 1 5AD7C17B
P 4000 5700
F 0 "#PWR08" H 4000 5550 50  0001 C CNN
F 1 "VCC" H 4000 5850 50  0000 C CNN
F 2 "" H 4000 5700 50  0000 C CNN
F 3 "" H 4000 5700 50  0000 C CNN
	1    4000 5700
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR09
U 1 1 5AD7C282
P 6200 5000
F 0 "#PWR09" H 6200 4850 50  0001 C CNN
F 1 "VCC" H 6200 5150 50  0000 C CNN
F 2 "" H 6200 5000 50  0000 C CNN
F 3 "" H 6200 5000 50  0000 C CNN
	1    6200 5000
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 5AD7C9AC
P 4300 6050
F 0 "C2" H 4325 6150 50  0000 L CNN
F 1 "100nF" H 4325 5950 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 4338 5900 50  0001 C CNN
F 3 "" H 4300 6050 50  0000 C CNN
	1    4300 6050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 5AD7CE77
P 3600 4500
F 0 "#PWR010" H 3600 4250 50  0001 C CNN
F 1 "GND" H 3600 4350 50  0000 C CNN
F 2 "" H 3600 4500 50  0000 C CNN
F 3 "" H 3600 4500 50  0000 C CNN
	1    3600 4500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 5AD7CF42
P 5900 4500
F 0 "#PWR011" H 5900 4250 50  0001 C CNN
F 1 "GND" H 5900 4350 50  0000 C CNN
F 2 "" H 5900 4500 50  0000 C CNN
F 3 "" H 5900 4500 50  0000 C CNN
	1    5900 4500
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 5AD7D33B
P 2100 4250
F 0 "C3" H 2125 4350 50  0000 L CNN
F 1 "100nF" H 2125 4150 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 2138 4100 50  0001 C CNN
F 3 "" H 2100 4250 50  0000 C CNN
	1    2100 4250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 5AD7D3E8
P 2100 4500
F 0 "#PWR012" H 2100 4250 50  0001 C CNN
F 1 "GND" H 2100 4350 50  0000 C CNN
F 2 "" H 2100 4500 50  0000 C CNN
F 3 "" H 2100 4500 50  0000 C CNN
	1    2100 4500
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 5AD7D55C
P 7600 3650
F 0 "C4" H 7625 3750 50  0000 L CNN
F 1 "100nF" H 7625 3550 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 7638 3500 50  0001 C CNN
F 3 "" H 7600 3650 50  0000 C CNN
	1    7600 3650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 5AD7D6D4
P 7600 3900
F 0 "#PWR013" H 7600 3650 50  0001 C CNN
F 1 "GND" H 7600 3750 50  0000 C CNN
F 2 "" H 7600 3900 50  0000 C CNN
F 3 "" H 7600 3900 50  0000 C CNN
	1    7600 3900
	1    0    0    -1  
$EndComp
$Comp
L Battery BT1
U 1 1 5AD832FC
P 4000 6050
F 0 "BT1" H 3850 6150 50  0000 L CNN
F 1 "18650" H 3900 5900 50  0000 L CNN
F 2 "myBatteries:18650" V 4000 6090 50  0001 C CNN
F 3 "" V 4000 6090 50  0000 C CNN
	1    4000 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 6300 5900 6300
Wire Wire Line
	2100 3750 2100 4100
Wire Wire Line
	2100 3900 3750 3900
Wire Wire Line
	5800 3750 7250 3750
Wire Wire Line
	7250 3750 7250 2900
Wire Wire Line
	7250 2900 7650 2900
Wire Wire Line
	8700 2900 8700 2250
Wire Wire Line
	8600 2900 8700 2900
Wire Wire Line
	7650 3050 7400 3050
Wire Wire Line
	8750 3050 8600 3050
Wire Wire Line
	5800 2850 7350 2850
Wire Wire Line
	7350 2850 7350 3200
Wire Wire Line
	7350 3200 7650 3200
Wire Wire Line
	5800 3000 7300 3000
Wire Wire Line
	7300 3000 7300 2550
Wire Wire Line
	7300 2550 8800 2550
Wire Wire Line
	8800 2550 8800 3200
Wire Wire Line
	8800 3200 8600 3200
Wire Wire Line
	7600 3350 7650 3350
Wire Wire Line
	8700 3350 8700 3550
Wire Wire Line
	8600 3350 8700 3350
Wire Wire Line
	8700 2250 3650 2250
Wire Wire Line
	3650 2250 3650 3000
Wire Wire Line
	3200 3000 3750 3000
Wire Wire Line
	7400 3050 7400 2200
Wire Wire Line
	7400 2200 3600 2200
Wire Wire Line
	3600 2200 3600 2850
Wire Wire Line
	3300 2850 3750 2850
Wire Wire Line
	8750 3050 8750 2150
Wire Wire Line
	8750 2150 3550 2150
Wire Wire Line
	3550 2150 3550 3150
Wire Wire Line
	3100 3150 3750 3150
Wire Wire Line
	5100 6100 5100 6300
Wire Wire Line
	5600 6200 5600 6400
Wire Wire Line
	5600 5700 5600 5900
Connection ~ 5600 5800
Connection ~ 5100 6300
Connection ~ 5600 6300
Wire Wire Line
	5500 5800 5600 5800
Wire Wire Line
	7600 2800 7600 3500
Wire Wire Line
	3800 6300 4550 6300
Wire Wire Line
	3800 5800 4700 5800
Wire Wire Line
	3400 1700 3400 2700
Wire Wire Line
	3400 2700 3750 2700
Wire Wire Line
	3300 1700 3300 2850
Connection ~ 3600 2850
Wire Wire Line
	3200 1700 3200 3000
Connection ~ 3650 3000
Wire Wire Line
	3100 1700 3100 3150
Connection ~ 3550 3150
Wire Wire Line
	3000 1700 3000 3300
Wire Wire Line
	3000 3300 3750 3300
Wire Wire Line
	2900 1700 2900 3450
Wire Wire Line
	2900 3450 3750 3450
Wire Wire Line
	2800 1700 2800 3600
Wire Wire Line
	2800 3600 3750 3600
Wire Wire Line
	2700 1700 2700 3750
Wire Wire Line
	2700 3750 3750 3750
Wire Wire Line
	2600 3900 2600 1700
Connection ~ 2600 3900
Wire Wire Line
	2500 1700 2500 4050
Wire Wire Line
	2500 4050 3750 4050
Wire Wire Line
	2400 4200 3750 4200
Wire Wire Line
	2400 4200 2400 1700
Wire Wire Line
	2300 1700 2300 4350
Wire Wire Line
	2300 4350 3750 4350
Wire Wire Line
	6000 1700 6000 2700
Wire Wire Line
	6000 2700 5800 2700
Wire Wire Line
	6100 2850 6100 1700
Connection ~ 6100 2850
Wire Wire Line
	6200 1700 6200 3000
Connection ~ 6200 3000
Wire Wire Line
	5800 3150 6300 3150
Wire Wire Line
	6300 3150 6300 1700
Wire Wire Line
	6400 1700 6400 3300
Wire Wire Line
	6400 3300 5800 3300
Wire Wire Line
	5800 3450 6500 3450
Wire Wire Line
	6500 3450 6500 1700
Wire Wire Line
	6600 1700 6600 3600
Wire Wire Line
	6600 3600 5800 3600
Wire Wire Line
	6700 3750 6700 1700
Connection ~ 6700 3750
Wire Wire Line
	6800 3900 6800 1700
Wire Wire Line
	5800 3900 6800 3900
Wire Wire Line
	5800 4050 6900 4050
Wire Wire Line
	6900 4050 6900 1700
Wire Wire Line
	7000 1700 7000 4200
Wire Wire Line
	7000 4200 5800 4200
Wire Wire Line
	5800 4350 7100 4350
Wire Wire Line
	7100 4350 7100 1700
Wire Wire Line
	3800 5800 3800 6000
Wire Wire Line
	3800 6000 3700 6000
Wire Wire Line
	3800 6300 3800 6100
Wire Wire Line
	3800 6100 3700 6100
Wire Wire Line
	4600 1700 4600 1950
Wire Wire Line
	4600 1950 4550 1950
Wire Wire Line
	4550 1950 4550 2300
Wire Wire Line
	4700 1700 4700 2300
Wire Wire Line
	4800 1700 4800 1950
Wire Wire Line
	4800 1950 4850 1950
Wire Wire Line
	4850 1950 4850 2300
Wire Wire Line
	4900 1700 4900 1900
Wire Wire Line
	4900 1900 5000 1900
Wire Wire Line
	5000 1900 5000 2300
Wire Wire Line
	2800 5800 2700 5800
Wire Wire Line
	2800 5100 2800 6000
Wire Wire Line
	2800 5700 2700 5700
Connection ~ 2800 5800
Wire Wire Line
	2700 5600 2800 5600
Connection ~ 2800 5700
Wire Wire Line
	2700 5500 2800 5500
Connection ~ 2800 5600
Wire Wire Line
	2700 5100 2800 5100
Connection ~ 2800 5500
Wire Wire Line
	2700 5200 2800 5200
Connection ~ 2800 5200
Wire Wire Line
	2700 5300 2800 5300
Connection ~ 2800 5300
Wire Wire Line
	2700 5400 2800 5400
Connection ~ 2800 5400
Wire Wire Line
	6400 5000 6400 5400
Wire Wire Line
	6400 5400 6500 5400
Wire Wire Line
	6500 5300 6400 5300
Connection ~ 6400 5300
Wire Wire Line
	6400 5200 6500 5200
Connection ~ 6400 5200
Wire Wire Line
	6500 5100 6400 5100
Connection ~ 6400 5100
Wire Wire Line
	6200 5000 6200 5800
Wire Wire Line
	6200 5800 6500 5800
Wire Wire Line
	6500 5700 6200 5700
Connection ~ 6200 5700
Wire Wire Line
	6200 5600 6500 5600
Connection ~ 6200 5600
Wire Wire Line
	6500 5500 6200 5500
Connection ~ 6200 5500
Wire Wire Line
	4300 5800 4300 5900
Connection ~ 4300 5800
Wire Wire Line
	4300 6200 4300 6300
Connection ~ 4300 6300
Wire Wire Line
	3600 4500 3600 4200
Connection ~ 3600 4200
Wire Wire Line
	5900 4500 5900 3900
Connection ~ 5900 3900
Wire Wire Line
	2100 4500 2100 4400
Connection ~ 2100 3900
Connection ~ 7600 3350
Wire Wire Line
	7600 3900 7600 3800
Wire Wire Line
	4000 5700 4000 5900
Connection ~ 4000 5800
Wire Wire Line
	4000 6200 4000 6300
Connection ~ 4000 6300
$Comp
L LM2931Z-5.0 U2
U 1 1 5AD840D2
P 5100 5850
F 0 "U2" H 5100 6150 50  0000 C CNN
F 1 "LM2936-3.3" H 5100 6050 50  0000 C CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 5100 5950 50  0001 C CIN
F 3 "" H 5100 5850 50  0000 C CNN
	1    5100 5850
	1    0    0    -1  
$EndComp
$Comp
L TEST_1P W1
U 1 1 5AD97B36
P 5900 6200
F 0 "W1" H 5900 6470 50  0000 C CNN
F 1 "GND" H 5900 6400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 6100 6200 50  0001 C CNN
F 3 "" H 6100 6200 50  0000 C CNN
	1    5900 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 6300 5900 6200
$EndSCHEMATC
