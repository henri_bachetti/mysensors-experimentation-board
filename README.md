# MYSENSORS EXPERIMENTATION BOARD

The purpose of this page is to explain step by step the realization of a low power experimentation board based on ARDUINO PRO MINI, using an NRF24L01 2.4GHZ module.

The board uses the following components :

 * an ARDUINO PRO MINI 3.3V 8MHz
 * a NRF24L01
 * a LM2936-3.3 regulator
 * some passive components
 * the board is powered by a 18650 LITHIUM-ION battery.

You can load in it the following sketch for exemple : https://www.mysensors.org/build/temp

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2018/04/carte-dexperimentation-mysensors.html